// script.js
document.addEventListener('DOMContentLoaded', () => {
    const samuraiLevelSlider = document.getElementById('samurai-level');
    const petLevelSlider = document.getElementById('pet-level');
    const samuraiSpdInput = document.getElementById('samurai-spd');
    const petSpdInput = document.getElementById('pet-spd');
    const riceBagInput = document.getElementById('rice-bag');

    const samuraiLevelLabel = document.getElementById('samurai-level-label');
    const petLevelLabel = document.getElementById('pet-level-label');

    const baseSamSpeed = document.getElementById('base-sam-speed');
    const basePetSpeed = document.getElementById('base-pet-speed');
    const baseRiceBag = document.getElementById('base-rice-bag');
    const baseDays = document.getElementById('base-limit');
    const baseRefund = document.getElementById('base-refund');

    const nftSamSpeed = document.getElementById('nft-sam-speed');
    const nftPetSpeed = document.getElementById('nft-pet-speed');
    const nftRiceBag = document.getElementById('nft-rice-bag');
    const nftDays = document.getElementById('nft-limit');
    const nftRefund = document.getElementById('nft-refund');

    const samuraiCost = document.getElementById('cost-sam');
    const petCost = document.getElementById('cost-pet');
    const totalCost = document.getElementById('total-cost');
    const totalUsd = document.getElementById('cost-usd');

    const nftRow = document.getElementById('nft-row');

    const copyTextButton = document.getElementById('copy-text');
    let jsonData, sssPriceUsd;

    // Fetch JSON data
    fetch('sss_source.json')
        .then(response => response.json())
        .then(data => {
            jsonData = data;
            console.log('JSON data loaded:', jsonData);
        })
        .catch(error => console.error('Error loading JSON data:', error));

    // Fetch price data from API SSS
    fetch('https://play-tele-api.sss.game/public/price')
        .then(response => response.json())
        .then(data => {
            sssPriceUsd = parseFloat(data.sss_usd);
            console.log('Price data loaded:', sssPriceUsd);
        })
        .catch(error => console.error('Error loading price data:', error));

    function calculateResults() {
        const samuraiLevel = parseInt(samuraiLevelSlider.value);
        const petLevel = parseInt(petLevelSlider.value);
        const samuraiSpd = parseInt(samuraiSpdInput.value);
        const petSpd = parseInt(petSpdInput.value);
        const riceBag = parseInt(riceBagInput.value);
        const maxRiceBag = 4509601296;
        const refundFactor = 333;

        if (!jsonData) {
            console.error('JSON data not loaded yet.');
            return;
        }

        // Get base values from JSON data
        const baseSamuraiSpeed = jsonData[samuraiLevel].samspeed;
        const basePetSpeedValue = jsonData[petLevel].petspeed;

        // Get cost value from JSON
        const accumulatedSamCost = jsonData[samuraiLevel].samaccumulatedcost;
        const accumulatedPetCost = jsonData[petLevel].petaccumulatedcost;
        const accumulatedTotalCost = parseInt(accumulatedSamCost) + parseInt(accumulatedPetCost)
        const accumulatedTotalCostUsd = accumulatedTotalCost * sssPriceUsd;

        // Calculation logic
        const baseFinalSpeedValue = Math.min(baseSamuraiSpeed,basePetSpeedValue);
        const baseDaysValue = maxRiceBag / baseFinalSpeedValue; // Example
        const baseDayRefund = accumulatedTotalCost/baseFinalSpeedValue/refundFactor;

        const nftSamuraiSpeed = baseSamuraiSpeed * (1 + (samuraiSpd/100));
        const nftPetSpeedValue = basePetSpeedValue * (1 + (petSpd/100));
        const nftFinalSpeedValue = Math.min(nftSamuraiSpeed, nftPetSpeedValue);
        const nftRiceBagValue = maxRiceBag * (1 + (riceBag/100))
        const nftDaysValue = nftRiceBagValue/nftFinalSpeedValue; // Example
        const nftDayRefund = accumulatedTotalCost/nftFinalSpeedValue/refundFactor;

        baseSamSpeed.textContent = baseSamuraiSpeed.toFixed(2);
        basePetSpeed.textContent = basePetSpeedValue.toFixed(2);
        baseRiceBag.textContent = formatNumber(maxRiceBag);
        baseRefund.textContent = convertSecondsToDhms(parseInt(baseDayRefund))
        baseDays.textContent = convertSecondsToDhms(parseInt((baseDaysValue)));

        nftSamSpeed.textContent = nftSamuraiSpeed.toFixed(2);
        nftPetSpeed.textContent = nftPetSpeedValue.toFixed(2);
        nftRiceBag.textContent = formatNumber(nftRiceBagValue);
        nftRefund.textContent = convertSecondsToDhms(parseInt(nftDayRefund));
        nftDays.textContent = convertSecondsToDhms(parseInt(nftDaysValue));

        samuraiCost.textContent = formatNumber(accumulatedSamCost);
        petCost.textContent = formatNumber(accumulatedPetCost);
        totalCost.textContent = formatNumber(accumulatedTotalCost);
        totalUsd.textContent = formatNumber(accumulatedTotalCostUsd);
        
        if (samuraiSpd + petSpd + riceBag > 0) {
            nftRow.style.display = '';
            nftRow.classList.add('highlight');
            baseSamSpeed.style.fontWeight = 'normal';
            baseSamSpeed.style.backgroundColor = '#fff';
            basePetSpeed.style.fontWeight = 'normal';      
            basePetSpeed.style.backgroundColor = '#fff';
            if (nftPetSpeedValue <= nftSamuraiSpeed) {
                nftSamSpeed.style.fontWeight = 'normal';
                nftSamSpeed.style.backgroundColor = 'peachpuff';
                nftPetSpeed.style.fontWeight = 'bold';
                nftPetSpeed.style.backgroundColor = 'aquamarine';
            } else {
                nftPetSpeed.style.fontWeight = 'normal';
                nftPetSpeed.style.backgroundColor = 'peachpuff';
                nftSamSpeed.style.fontWeight = 'bold';
                nftSamSpeed.style.backgroundColor = 'aquamarine';
            }
        } else {
            nftRow.style.display = 'none';
            if (basePetSpeedValue <= baseSamuraiSpeed) {
                baseSamSpeed.style.fontWeight = 'normal';
                baseSamSpeed.style.backgroundColor = '#fff';
                basePetSpeed.style.fontWeight = 'bold';
                basePetSpeed.style.backgroundColor = 'aquamarine';
            } else {
                basePetSpeed.style.fontWeight = 'normal';
                basePetSpeed.style.backgroundColor = '#fff';
                baseSamSpeed.style.fontWeight = 'bold';
                baseSamSpeed.style.backgroundColor = 'aquamarine';
            }
        }
    }

    function updateSliderLabel(slider, label) {
        const value = slider.value;
        label.textContent = value;
        const sliderWidth = slider.offsetWidth;
        const labelWidth = label.offsetWidth;
        const max = slider.max;
        const left = (value / max) * (sliderWidth - labelWidth);
        label.style.left = `${left}px`;
    }

    function validateInput(input) {
        let value = parseInt(input.value);
        if (isNaN(value) || value < 0) {
            input.value = 0;
        } else {
            input.value = value;
        }
    }

    function formatNumber(num) {
        if (num >= 1e9) {
            return (num / 1e9).toFixed(1) + 'B';
        } else if (num >= 1e6) {
            return (num / 1e6).toFixed(1) + 'M';
        } else if (num >= 1e3) {
            return (num / 1e3).toFixed(1) + 'K';
        } else {
            return num.toFixed(2).toLocaleString();
        }
    }
    

    function convertSecondsToDhms(seconds) {
        const days = Math.floor(seconds / (24 * 60 * 60));
        seconds %= (24 * 60 * 60);
        const hours = Math.floor(seconds / (60 * 60));
        seconds %= (60 * 60);
        const minutes = Math.floor(seconds / 60);
        seconds %= 60;
        return `${days} days ${String(hours).padStart(2, '0')}:${String(minutes).padStart(2, '0')}:${String(seconds).padStart(2, '0')}`;    
    }

    samuraiLevelSlider.addEventListener('input', () => {
        samuraiLevelLabel.textContent = samuraiLevelSlider.value;
        updateSliderLabel(samuraiLevelSlider, samuraiLevelLabel);
        calculateResults();
    });

    petLevelSlider.addEventListener('input', () => {
        petLevelLabel.textContent = petLevelSlider.value;
        updateSliderLabel(petLevelSlider, petLevelLabel);
        calculateResults();
    });

    samuraiSpdInput.addEventListener('input', () => {
        validateInput(samuraiSpdInput);
        calculateResults();
    });

    petSpdInput.addEventListener('input', () => {
        validateInput(petSpdInput);
        calculateResults();
    });

    riceBagInput.addEventListener('input', () => {
        validateInput(riceBagInput);
        calculateResults();
    });

    copyTextButton.addEventListener('click', () => {
        const textToCopy = "0x85E1D9512A7eA9271C9418f9E6E61e9082948815";
        navigator.clipboard.writeText(textToCopy).then(() => {
        }).catch(err => {
            console.error('Failed to copy text: ', err);
        });
    });

    samuraiLevelSlider.value = samuraiLevelSlider.min;
    petLevelSlider.value = petLevelSlider.min;
    samuraiLevelLabel.textContent = samuraiLevelSlider.min;
    petLevelLabel.textContent = petLevelSlider.min;
    samuraiSpdInput.value = samuraiSpdInput.min
    petSpdInput.value = petSpdInput.min
    riceBagInput.value = riceBagInput.min
    updateSliderLabel(samuraiLevelSlider, samuraiLevelLabel);
    updateSliderLabel(petLevelSlider, petLevelLabel);
});